﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E4BIGSCHOOL2.Models
{
    public class Category
    {
        public byte Id { get; set; } // Unique identifier for the category
        [Required]
        [StringLength(255)]
        public string Name { get; set; } // Name of the category
    }
}