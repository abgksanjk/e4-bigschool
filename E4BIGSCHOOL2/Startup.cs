﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(E4BIGSCHOOL2.Startup))]
namespace E4BIGSCHOOL2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
