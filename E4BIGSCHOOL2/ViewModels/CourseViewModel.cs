﻿using E4BIGSCHOOL2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E4BIGSCHOOL2.ViewModels
{
    public class CourseViewModel
    {
        public string Place { get; set; }
        public string Date { get; set; }
        public string Time { get; set;}
        public byte Category { get; set; }
        public IEnumerable<Category> Categories { get; set; } 

        public DateTime GetDateTime()
        {
            return DateTime.ParseExact($"{Date} {Time}", "MM/dd/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
        }
    }
}